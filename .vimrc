" Load settings
source ~/.vim/settings.vim

" Load plugins
source ~/.vim/plugins.vim

" Script contains mappings
source ~/.vim/mappings.vim
