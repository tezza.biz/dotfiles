call plug#begin()
  Plug 'https://github.com/tpope/vim-surround'
  Plug 'https://github.com/preservim/nerdtree',
  Plug 'https://github.com/ap/vim-css-color',
  Plug 'https://github.com/tpope/vim-commentary'
  Plug 'junegunn/fzf', { 'do': { ->fzf#install() } }
  Plug 'junegunn/fzf.vim'
call plug#end()
